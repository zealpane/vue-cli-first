const routers = [
	{
	    path: '/',
	    meta: {
	        title: ''
	    },
	    component: (resolve) => require(['./views/index.vue'], resolve)
	},
	{
	    path: '/second',
	    meta: {
	        title: '第二个页面'
	    },
	    component: (resolve) => require(['./views/second.vue'], resolve)
	},
	{
	    path: '/table',
	    meta: {
	        title: '第二个页面'
	    },
	    component: (resolve) => require(['./pages/table.vue'], resolve)
	}
];
export default routers;